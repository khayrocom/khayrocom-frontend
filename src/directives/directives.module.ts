import { NgModule } from '@angular/core';
import { SwipeSegmentDirective } from './swipe-segment/swipe-segment';
@NgModule({
	declarations: [SwipeSegmentDirective],
	imports: [],
	exports: [SwipeSegmentDirective],
	providers :[SwipeSegmentDirective]
})
export class DirectivesModule {}

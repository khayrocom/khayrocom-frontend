import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { khayrocom } from './app.component';
import { HomePage } from '../pages/home/home';
import { HomePageModule } from '../pages/home/home.module';
import {LoginPage} from "../pages/login/login";
import {LoginPageModule} from "../pages/login/login.module";
import {RegistrationPageModule} from "../pages/registration/registration.module";
import {RegistrationPage} from "../pages/registration/registration";
import {ForgetpasswordPageModule} from "../pages/forgetpassword/forgetpassword.module";
import {ForgetpasswordPage} from "../pages/forgetpassword/forgetpassword";
import {MenuPageModule} from "../pages/menu/menu.module";
import {MenuPage} from "../pages/menu/menu";
import {AddClassPageModule} from "../pages/add-class/add-class.module"
import {AddClassPage} from "../pages/add-class/add-class";
import {UpdateuserPage} from "../pages/updateuser/updateuser";
import {UpdateuserPageModule} from "../pages/updateuser/updateuser.module";
import {UpdatestudentPage} from "../pages/updatestudent/updatestudent";
import {UpdatestudentPageModule} from "../pages/updatestudent/updatestudent.module";
import {UpdateinstitutePage} from "../pages/updateinstitute/updateinstitute";
import {UpdateinstitutePageModule} from "../pages/updateinstitute/updateinstitute.module";
import {UpdateclassPage} from "../pages/updateclass/updateclass";
import {UpdateclassPageModule} from "../pages/updateclass/updateclass.module";

import {AddUserPageModule} from "../pages/add-user/add-user.module";
import {AddUserPage} from "../pages/add-user/add-user";

import {UserInfoPageModule} from "../pages/user-info/user-info.module";
import {UserInfoPage} from "../pages/user-info/user-info"

import {AchievementListPageModule} from "../pages/achievement-list/achievement-list.module";
import {AchievementListPage} from "../pages/achievement-list/achievement-list";


import {MenupPageModule} from "../pages/menup/menup.module";
import {MenupPage} from "../pages/menup/menup";

import {AddAchievementPageModule}  from "../pages/add-achievement/add-achievement.module";
import {AddAchievementPage} from "../pages/add-achievement/add-achievement";

import {AddInstitutePageModule}  from "../pages/add-institute/add-institute.module";
import {AddInstitutePage} from "../pages/add-institute/add-institute";


import {AccountInfoPageModule}  from "../pages/account-info/account-info.module";
import {AccountInfoPage} from "../pages/account-info/account-info";


import {AccountInfoEditPageModule}  from "../pages/account-info-edit/account-info-edit.module";
import {AccountInfoEditPage} from "../pages/account-info-edit/account-info-edit";

import {InstituteListPageModule}  from "../pages/institute-list/institute-list.module";
import {InstituteListPage} from "../pages/institute-list/institute-list";

import {ClassListPageModule}  from "../pages/class-list/class-list.module";
import {ClassListPage} from "../pages/class-list/class-list";

import {ReportPageModule} from "../pages/report/report.module";
import {ReportPage} from "../pages/report/report";

import {LogoutPage} from "../pages/logout/logout";
import {LogoutPageModule} from "../pages/logout/logout.module";

import {UserListPageModule}  from "../pages/user-list/user-list.module";
import {UserListPage} from "../pages/user-list/user-list";

import {HttpModule} from "@angular/http";
import { Geolocation } from '@ionic-native/geolocation/ngx';


import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
//import { RouterModule } from '@angular/router';
//import {Storage} from "@ionic/storage";
import { IonicStorageModule } from '@ionic/storage';


import { SwipeSegmentDirective } from '../directives/swipe-segment/swipe-segment';
import { DirectivesModule } from '../directives/directives.module';

import { AuthProvider } from '../providers/auth/auth';


import {ResetpasswordPage} from "../pages/resetpassword/resetpassword";
import {ResetpasswordPageModule} from "../pages/resetpassword/resetpassword.module";

import {PwcodevalidationPage} from "../pages/pwcodevalidation/pwcodevalidation";
import {PwcodevalidationPageModule} from "../pages/pwcodevalidation/pwcodevalidation.module";
import {StudentListPage} from "../pages/student-list/student-list";
import {StudentListPageModule} from "../pages/student-list/student-list.module";
import {AddStudentPage} from "../pages/add-student/add-student";
import {AddStudentPageModule} from "../pages/add-student/add-student.module";
import {HalaqaListPageModule} from "../pages/halaqa-list/halaqa-list.module";
import {HalaqaListPage} from "../pages/halaqa-list/halaqa-list";
import {AddHalaqaPage} from "../pages/add-halaqa/add-halaqa";
import {AddHalaqaPageModule} from "../pages/add-halaqa/add-halaqa.module";

// const routes: Routes = [
//   { path: '',redirectTo:'login',pathMatch:'full'},
//   { path: 'login',component: LoginPage },
//   { path: 'registre',component: RegistrationPage },
//   //{ path: 'home', component: HomePage },
//   { path: 'institutes',component: InstituteListPage },
//   { path: 'institutes/classs',component: ClassListPage },
//   { path: 'institutes/classs/student',component: UserListPage },
//   { path: 'student/achievement',component: AchievementListPage },
//   { path: 'add/institute',component: AddInstitutePage },
//   { path: 'add/class',component: AddClassPage },
//   { path: 'add/student',component: AddUserPage },
//   { path: 'add/achievement',component: AddAchievementPage  }
// ];
@NgModule({
  declarations: [
    khayrocom,

    //SwipeSegmentDirective
  ],
  imports: [//RouterModule.forChild(routes),//,HomePageModule,
    DirectivesModule,HomePageModule,//SwipeSegmentDirective,
    BrowserModule,IonicModule.forRoot(khayrocom),HttpModule,LoginPageModule,RegistrationPageModule,ForgetpasswordPageModule,
    MenuPageModule,AddClassPageModule, AddUserPageModule, AddAchievementPageModule,AddInstitutePageModule,MenupPageModule,
    AccountInfoPageModule,AccountInfoEditPageModule,PwcodevalidationPageModule,ResetpasswordPageModule,
    InstituteListPageModule, ClassListPageModule, UserListPageModule,LogoutPageModule,UpdateuserPageModule,
    UpdateclassPageModule,UpdateinstitutePageModule,UpdatestudentPageModule,StudentListPageModule,AddStudentPageModule,
    UserInfoPageModule,AchievementListPageModule,ReportPageModule, IonicStorageModule.forRoot(),HalaqaListPageModule,
      AddHalaqaPageModule


  ],
  exports:[SwipeSegmentDirective,IonicStorageModule],//RouterModule
  bootstrap: [IonicApp],//
  entryComponents: [
    khayrocom,HomePage,LoginPage,ReportPage,ResetpasswordPage,PwcodevalidationPage,LogoutPage,UpdateuserPage,
    RegistrationPage,ForgetpasswordPage,MenuPage,AddInstitutePage,AddClassPage,AddUserPage,AddAchievementPage,
    MenupPage,AccountInfoPage,AccountInfoEditPage,InstituteListPage,ClassListPage,UserListPage,UserInfoPage,AchievementListPage,
    UpdateclassPage,UpdateinstitutePage,UpdatestudentPage,StudentListPage,AddStudentPage,HalaqaListPage,AddHalaqaPage
  ],

  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    File,
    FileOpener,
    IonicStorageModule,
    //  LogoutPage,
    AuthProvider,
  ],

})
export class AppModule {}

import { Component   , ViewChild  } from '@angular/core';
import { App,Platform ,MenuController,Nav} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import {LoginPage} from "../pages/login/login";
import {LogoutPage} from "../pages/logout/logout";
import {HomePage} from "../pages/home/home";
import {InstituteListPage} from "../pages/institute-list/institute-list";
import {UserListPage} from "../pages/user-list/user-list";
import {ReportPage} from "../pages/report/report";

import {Http} from "@angular/http";
import {env} from '../environnement'
import {AuthProvider} from "../providers/auth/auth";
@Component({
  templateUrl: 'app.html'
})

export class khayrocom {
   rootPage:any ;

   URL=env.URL;
   @ViewChild(Nav) nav: Nav;
    role:any ;//localStorage.getItem("role");
//  rootPage:any = LoginPage;
pages:Array<any> = [
      {title: 'Home', component: HomePage, icon: 'home'},
      {title: 'Profile', component: HomePage, icon: 'contact'},
      {title: 'Institutes', component: InstituteListPage, icon: 'person'},
      {title: 'Statistics', component: HomePage, icon: 'stats'},
    {title: 'Report', component: ReportPage, icon: 'document'},
      {title: 'Logout', component: LogoutPage, icon: 'log-out'}
    ];


    pages1:Array<any> = [
        {title: 'Home', component: HomePage, icon: 'home'},
        {title: 'Profile', component: HomePage, icon: 'contact'},
        {title: 'Institutes', component: InstituteListPage, icon: 'person'},
        {title: 'Statistics', component: HomePage, icon: 'stats'},
        {title: 'Report', component: ReportPage, icon: 'document'},
        {title: 'Users', component: UserListPage, icon: 'contact'},
        {title: 'Logout', component: LogoutPage, icon: 'log-out'}
    ];
  constructor(public auth:AuthProvider,
              platform: Platform,private http:Http,
              statusBar: StatusBar,splashScreen: SplashScreen,
              menuCtrl:MenuController) {

      this.auth.getrole.subscribe((role)=>{
          this.role=role;
      });

    platform.ready().then(() => {
   //   // Okay, so the platform is ready and our plugins are available.
   //   // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    menuCtrl.enable(true,"menu");

    this.http.get(this.URL+"verifytoken/"+localStorage.getItem('token')).map(res => res.json())
    .subscribe(dataverify =>{
      console.log('before if ,token verifytoken',dataverify);
            localStorage.setItem("iduser",dataverify.iduser);
            localStorage.setItem("role",dataverify.role);
            //this.role=localStorage.getItem("role");
            this.auth.getrole.next(dataverify.role);
            let newtokentolist=localStorage.getItem("listtoken");
            if(!newtokentolist.match(localStorage.getItem('token'))){
                newtokentolist=localStorage.getItem('token') +","+newtokentolist;
                localStorage.setItem("listtoken",newtokentolist);
            }

            if(dataverify.success==true){
  //  console.log('token verifytoken ',dataverify);
    //this.navCtrl.push(InstituteListPage,{data:dataverify.iduser});
  this.rootPage = HomePage  ;

  }
  else{

    //localStorage.clear();
    //localStorage.reset();
    this.rootPage = LoginPage;

  }
},
err=>{
  this.rootPage = LoginPage;

});

  }
  openPage(page) {
    console.log(page);
    if(page.component==InstituteListPage) {
        this.auth.checkuserpermission.next('false');
    this.auth.checkinstituteasadmin.next('true');
    }

      this.nav.push(page.component);
    }
}

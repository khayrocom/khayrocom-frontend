//import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {Http, RequestOptions} from '@angular/http';
import {Headers} from "@angular/http";
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {Header} from "ionic-angular/umd/navigation/nav-interfaces";

import {JwtHelper} from "angular2-jwt";
import {Storage} from "@ionic/storage";
import {AuthService} from "./AuthService";

import {env} from '../../environnement'
import {BehaviorSubject} from "rxjs";

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {
  public getrole = new BehaviorSubject('');//
  public checkuserpermission = new BehaviorSubject('false');//
  public checkinstituteasadmin= new BehaviorSubject('false');

  //public token: any;
URL=env.URL;
  contentHeader = new Headers({"Content-Type": "application/json"});
  error: string;
  user: string;
  jwtHelper = new JwtHelper();
  auth: AuthService;
  constructor(public http: Http,private storage: Storage) {
    this.auth = AuthService;

    storage.ready().then(() => {
      storage.get('profile').then(profile => {
        this.user = JSON.parse(profile);
      }).catch(console.log);
    });

  }

  login(data){
    // return new Promise((resolve,reject)=>{
    //
    //   this.http.post(this.apiUrl+'login',identifiant)
    //     .subscribe(res=>{
    //       resolve(res.json());
    //     },(err)=>{
    //       reject(err);
    //     });
    //
    // });
    let account={
      email:data.name,
      password:data.password
    };
    //console.log(account);
    return this.http.post(this.URL+"login", account)
      ;

  }
}

import {Component, Provider} from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Http, Response} from "@angular/http";
import {AddClassPage} from "../add-class/add-class";
import {UserListPage} from "../user-list/user-list";
import {UpdateclassPage} from "../updateclass/updateclass";

import 'rxjs/add/operator/map';

import {env} from '../../environnement'
import {StudentListPage} from "../student-list/student-list";
import {AddHalaqaPage} from "../add-halaqa/add-halaqa";
import {ClassListPage} from "../class-list/class-list";
import {AuthProvider} from "../../providers/auth/auth";

/**
 * Generated class for the HalaqaListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-halaqa-list',
  templateUrl: 'halaqa-list.html',
})
export class HalaqaListPage {
  data: any = [];
  ListHalaqa: any [] = [];
  Institute: any;
  URL = env.URL;
role:any;
  public isSearchbarOpened = false;

  public category: string = 'LIST';
  public categories: Array<string> = ['LIST', 'INFO'];

  constructor(private LoadincC: LoadingController,
              private http: Http,
              private AlertC: AlertController,
              public navCtrl: NavController,
              public navParams: NavParams,
              public auth:AuthProvider) {
    this.Institute = navParams.get('data');

    console.log("class recived Nav params", this.Institute);
    localStorage.setItem("Institute", this.Institute.idinstitute);


  }

  ionViewWillEnter() {
      this.auth.checkuserpermission.next('false');
      this.auth.getrole.subscribe((role)=>this.role=role);

//this.ListInstituesNames = [];
    this.gethalaqa();
  }

  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad HalaqaListPage');
  // }
  onSearch(ev) {
    this.http.get(this.URL + "halaqasearchbyinstitute?q=" + ev.target.value + "&idinstitute=" + this.Institute.idinstitute)
        .map(res => res.json())
        .subscribe(data => {
          let filtredlistname: any[] = [];

          data.forEach(x => {
            console.log("result", x);
            filtredlistname.unshift({HalaqaName: x.HalaqaName, idhalaqa: x._id});

          });
          this.ListHalaqa = filtredlistname;

        })

    //  console.log(ev.target.value);
  }
  goToAddHalaqa() {
    console.log('add halaqa page');
    this.navCtrl.push(AddHalaqaPage, {
      data: {
        'InstituteName': this.Institute.InstituteName,
        'idinstitute': this.Institute.idinstitute
      }
    });
  }

  GoToClassStudent(HalaqaName: any, _id: any) {
    console.log('add institute page');
      localStorage.setItem("idhalaqa",_id);

      this.navCtrl.push(StudentListPage,
        {data: {'HalaqaName': HalaqaName, 'idhalaqa': _id}});
  }

  gethalaqa() {
    console.log(this.URL + "halaqabyinstituteid/" + this.Institute.idinstitute);
    this.http.get(this.URL + "halaqabyinstituteid/" + localStorage.getItem('Institute') + "/" + localStorage.getItem('token'))
        .map(res => res.json())
        .subscribe(data => {
              this.ListHalaqa = [];
              data.forEach(x => this.ListHalaqa.unshift({HalaqaName: x.HalaqaName, idhalaqa: x._id}));

              console.log("this.ListClassNames", this.ListHalaqa);
              console.log("****************", data)
            },
            err => {
              let alert = this.AlertC.create(
                  {
                    title: "ERROR",
                    message: err,
                    buttons: [
                      {
                        text: "OK",
                        role: "cancel"

                      }
                    ]
                  }
              );
              alert.present();
            })

    //.map((res:Response ) =>  res.json())//.subscribe(data => { console.log("****************"   ,data);})
  }
}

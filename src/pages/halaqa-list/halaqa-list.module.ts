import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HalaqaListPage } from './halaqa-list';

@NgModule({
  declarations: [
    HalaqaListPage,
  ],
  imports: [
    IonicPageModule.forChild(HalaqaListPage),
  ],
})
export class HalaqaListPageModule {}

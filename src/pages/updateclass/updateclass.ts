import { Component } from '@angular/core';
import {env} from '../../environnement'
import 'rxjs/add/operator/map';
import {AlertController, IonicPage, LoadingController, NavController, NavParams,MenuController } from 'ionic-angular';
import {Http,Response} from "@angular/http";
/**
 * Generated class for the UpdateclassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-updateclass',
  templateUrl: 'updateclass.html',
})
export class UpdateclassPage {
  URL=env.URL;
  idclass:any;
  class:any[]=[];
  constructor(private LoadincC:LoadingController,
    private http:Http,
    private AlertC:AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
  private menu: MenuController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdateclassPage');
    this.idclass = this.navParams.get('data');

    this.http.get(this.URL+"classinfo?id="+this.idclass+"&token="+localStorage.getItem('token'))
    .map(res => res.json())
    .subscribe(data =>{
      this.class=data;
console.log('update',data);
    });
  }
  updateclass(idclass:any){


    const confirm = this.AlertC.create({
     title: 'Confirmation Alert',
     message: 'do you really want to change the information about this class?',
     buttons: [
       {
         text: 'cancel',
         handler: () => {
           console.log('Disagree clicked');

         }
       },
       {
         text: 'Agree',
         handler: () => {
           console.log('Agree clicked');
    this.http.put(this.URL+"classupdate?id="+idclass+"&token="+localStorage.getItem('token'),this.class)
    .subscribe(data=>{
      console.log('is class updated',data);
    })
  console.log('class updated=',this.class);
  this.navCtrl.pop();

}
}
]
});
confirm.present();


}
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateclassPage } from './updateclass';

@NgModule({
  declarations: [
    UpdateclassPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdateclassPage),
  ],
})
export class UpdateclassPageModule {}

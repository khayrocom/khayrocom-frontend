import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenupPage } from './menup';

@NgModule({
  declarations: [
    MenupPage,
  ],
  imports: [
    IonicPageModule.forChild(MenupPage),
  ],
})
export class MenupPageModule {}

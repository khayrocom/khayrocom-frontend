import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AlertController, IonicPage, LoadingController, NavController, NavParams,Platform} from 'ionic-angular';

import {Http} from "@angular/http";
import {env} from '../../environnement'
import {PwcodevalidationPage} from '../pwcodevalidation/pwcodevalidation'
/**
 * Generated class for the ForgetpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgetpassword',
  templateUrl: 'forgetpassword.html',
})
export class ForgetpasswordPage {
	URL=env.URL;
  data: any =[];
    email :any;

usercompte: any [] = [];
  constructor(private LoadincC:LoadingController,
  private http:Http,
  private AlertC:AlertController,
  public navCtrl: NavController,
  public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgetpasswordPage');
  }

  goBack(){
        this.navCtrl.pop();
    }
passreset(){
      let loader = this.LoadincC.create({content:"Loading"});
      loader.present().then(()=>{
        this.http.post(this.URL+"resetpw",{email:this.email.replace(/\s/g,'').toLowerCase()}).map(res => res.json())
  .subscribe(data => {// console.log(data); //data.forEach(x => this.usercompte.push(x));
  	//console.log("this sheet",this.email);

this.navCtrl.push(PwcodevalidationPage,{data:this.email.replace(/\s/g,'').toLowerCase()});
            loader.dismissAll();
          },
          err => {
            let alert = this.AlertC.create(
              {
                title:"ERROR",
                message:err,
                buttons: [
                  {
                    text:"OK",
                    role:"cancel"

                  }
                ]
              }
            );
            alert.present();
          }
        )
      })
    }


}

import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AlertController, IonicPage, LoadingController, NavController, NavParams,Platform} from 'ionic-angular';
import {LoginPage} from '../login/login'
import CryptoJS from 'crypto-js';

import {Http} from "@angular/http";
import {env} from '../../environnement'

/**
 * Generated class for the ResetpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resetpassword',
  templateUrl: 'resetpassword.html',
})
export class ResetpasswordPage {
	URL=env.URL;
  data: any =[];
   Password:any;
  RPassword:any;
    email :any;
code: any;
verification: any =[];
  constructor(private LoadincC:LoadingController,
  private http:Http,
  public AlertC:AlertController,
  public navCtrl: NavController,
  public navParams: NavParams) {
  	this.email=navParams.get('data');
  	console.log("this sheet",this.email);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetpasswordPage');
  }

  goBack(){
      this.navCtrl.pop();
  } 
submitreset(){

	    if(this.RPassword != this.Password){

      let alert = this.AlertC.create(
        {
          title:"Verify Password",
          message:"please verify that both passwords are identical",
          buttons: [
            {
              text:"OK",
              role:"cancel",
              handler: ()=>{
                this.Password ="";
                this.RPassword ="";
              }
            }
          ]
        }
      );
      alert.present();
    }
    else{
    let hashpassword = CryptoJS.SHA256(this.Password).toString(CryptoJS.enc.Hex);
      let loader = this.LoadincC.create({content:"Loading"});
      loader.present().then(()=>{
        this.http.post(this.URL+"endresetpw",{email:this.email,password:hashpassword}).map(res => res.json())
  .subscribe(data=>{//function(data:response) {if (data.verification)

  	console.log('reset or not',data);
  	if (data.message=="user updated")
{this.navCtrl.push(LoginPage);
	loader.dismissAll();}


 },err=> {
let alert = this.AlertC.create(
              {
                title:"ERROR",
                message:err,
                buttons: [
                  {
                    text:"OK",
                    role:"cancel"

                  }
                ]
              }
            );
             }
)
    }
    )
	}}

}

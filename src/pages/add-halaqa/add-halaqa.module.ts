import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddHalaqaPage } from './add-halaqa';

@NgModule({
  declarations: [
    AddHalaqaPage,
  ],
  imports: [
    IonicPageModule.forChild(AddHalaqaPage),
  ],
})
export class AddHalaqaPageModule {}

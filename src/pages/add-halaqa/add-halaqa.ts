import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';

import {Http} from "@angular/http";
import {ClassListPage} from "../class-list/class-list";
import {env} from '../../environnement'
import {HalaqaListPage} from "../halaqa-list/halaqa-list";


/**
 * Generated class for the AddHalaqaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-halaqa',
  templateUrl: 'add-halaqa.html',
})
export class AddHalaqaPage {
  InstituteName: any;
  Institute: any;
  URL=env.URL;
  HalaqaName:any;
  constructor(private LoadincC:LoadingController,private http:Http,
              private AlertC:AlertController,public navCtrl: NavController,
              public navParams: NavParams) {
    this.Institute = navParams.get('data');
    console.log("Institute name Nav params",this.Institute);


  }
  goToHalaqaList(){
    console.log('class list page')
    this.navCtrl.push(HalaqaListPage,{ data: {'InstituteName':this.Institute.InstituteName,
        'idinstitute':this.Institute.idinstitute}});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddHalaqaPage');
  }
  AddHalaqa(){

    let loader = this.LoadincC.create({content:"Loading"})
    loader.present().then(()=>{
      this.http.post(this.URL+"halaqa",{HalaqaName:this.HalaqaName,idinstitute:this.Institute.idinstitute,
        iduser:localStorage.getItem('iduser'),token:localStorage.getItem('token')})
          .subscribe(
          data  =>{
            console.log(data);
            loader.dismissAll();
            this.navCtrl.pop();

          },
          err => {
            let alert = this.AlertC.create(
                {
                  title:"ERROR",
                  message:err,
                  buttons: [
                    {
                      text:"OK",
                      role:"cancel"

                    }
                  ]
                }
            );
            alert.present();
          }
      )
    })
  }

}

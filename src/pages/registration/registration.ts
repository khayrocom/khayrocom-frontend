import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {env} from '../../environnement'
import CryptoJS from 'crypto-js';
import {LoginPage} from "../login/login";
import { FormGroup, FormControl, Validators,FormBuilder } from '@angular/forms';

import {Http} from "@angular/http";
import {forEach} from "@angular/router/src/utils/collection";
//import { HttpClient, HttpHeaders } from '@angular/common/http';

/*
import { Http, Headers, RequestOptions } from '@angular/http';
*/
/**
 * Generated class for the RegistrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})

//


export class RegistrationPage {
  URL=env.URL;
    iduser:any;
    Institutes:any []=[];
    registrationForm: FormGroup;
    ListInstitutes:any []=[];
  UserName :any;
  Password:any;
  Email:any;
  RPassword:any;
  constructor(private LoadincC:LoadingController,private http: Http,
              private AlertC:AlertController,
              public formBuilder:FormBuilder,
              public navCtrl: NavController,
              public navParams: NavParams) {
this.http.get(this.URL+"institute/"+localStorage.getItem("token"))
    .map(res => res.json())
    .subscribe(
        data =>
        {         this.ListInstitutes=[];

            data.forEach(x =>  this.ListInstitutes.unshift({InstituteName:x.InstituteName,idinstitute:x._id}));
            console.log("this.ListInstituesNames",this.ListInstitutes);
            console.log("****************"   ,data)},
        err => {
            let alert = this.AlertC.create(
                {
                    title:"ERROR",
                    message:err,
                    buttons: [
                        {
                            text:"OK",
                            role:"cancel"

                        }
                    ]
                }
            );
            alert.present();
        });

      //  console.log(ev.target.value);
     this.registrationForm = formBuilder.group({
  email: new FormControl('', Validators.compose([
    Validators.required,
    Validators.pattern('^[^\\s@]+@[^\\s@]+\\.[^\\s@]{2,}$'),//('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
     Validators.minLength(1)
  ]))})
  }
  goBack(){
        this.navCtrl.pop();
    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationPage');
  }

  register(){

    if(this.RPassword != this.Password){

      let alert = this.AlertC.create(
        {
          title:"Verify Password",
          message:"please verify that both passwords are identical",
          buttons: [
            {
              text:"OK",
              role:"cancel",
              handler: ()=>{
                this.Password ="";
                this.RPassword ="";
              }
            }
          ]
        }
      );
      alert.present();
    }
    else  if (this.registrationForm.valid){
      let hashpassword = CryptoJS.SHA256(this.Password).toString(CryptoJS.enc.Hex);

      console.log("hashpassword",hashpassword);

        const promise = new Promise((resolve, reject) => {
                this.http.post(this.URL+"users",{username:this.UserName,email:this.Email.replace(/\s/g,'').toLowerCase(),
            password:hashpassword}).subscribe(
          data  =>{
            console.log(data);
            this.iduser=data.json().result._id;
            //this.data1=data;
//if (data.json().success=="false" ){

     let loader1 =  this.LoadincC.create({content:"successful register ",duration: 3000});
       loader1.present();
      //localStorage.setItem('token', data.json().token);
         // this.navCtrl.push(LoginPage);
            loader1.dismissAll();
              resolve(this.iduser);
              this.navCtrl.pop();
          },
          err => {
            let alert = this.AlertC.create(
              {
                title:"ERROR",
                message:err,
                buttons: [
                  {
                    text:"OK",
                    role:"cancel",
                    handler: ()=>{
                      this.Password ="";
                      this.RPassword ="";
                    }
                  }
                ]
              }
            );
            alert.present();
            reject('false');
          }


    ) }).then((iduser)=>{
                    this.Institutes.forEach(x=>{this.http.put(this.URL+"Upinstlistuser?id="+x+"&token="+localStorage.getItem('token'),
                        {idusers:iduser})
                        .subscribe(data=>{
                            console.log('is institute updated',data);
                        })}
                    )
                });



    }
          else {
let alert =  this.AlertC.create({
message:"Invalide Email",
  buttons: [
                  {
                    text:"OK",
                    role:"cancel"}]});
            alert.present();
  }

  }
}

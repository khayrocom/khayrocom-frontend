import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';

import {Http} from "@angular/http";

import {InstituteListPage} from "../institute-list/institute-list";
import {env} from '../../environnement'

/**
 * Generated class for the AddInstitutePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-institute',
  templateUrl: 'add-institute.html',
})
export class AddInstitutePage {
  URL=env.URL;
iduser:any;
  InstituteName :any;
  InstituteCountry:any;
  InstituteCity:any;
 // HalaqaName:any;
  constructor(private LoadincC:LoadingController,private http:Http,private AlertC:AlertController,
    public navCtrl: NavController, public navParams: NavParams) {
      this.iduser = navParams.get('data');
 console.log("Institue name Nav params",this.iduser);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddInstitutePage');
  }

  goBack(){
        this.navCtrl.pop();
    }
  goToInstituteList(){
  	console.log('institute list page')
    this.navCtrl.push(InstituteListPage,{data:this.iduser});
  }
goToAdd(){

      let loader = this.LoadincC.create({content:"Loading"})
      loader.present().then(()=>{
          //const promise = new Promise((resolve, reject) => {
        this.http.post(this.URL+"institute",{
            InstituteName:this.InstituteName,
            Country:this.InstituteCountry,
            City:this.InstituteCity,
           // Halaqaname:this.HalaqaName,
            idusers:this.iduser,
            token:localStorage.getItem('token')}).subscribe(
          data  =>{
            console.log(data);
            loader.dismissAll();
            this.navCtrl.pop();

          },
          err => {
            let alert = this.AlertC.create(
              {
                title:"ERROR",
                message:err,
                buttons: [
                  {
                    text:"OK",
                    role:"cancel"

                  }
                ]
              }
            );
            alert.present();
          }
        )
          // }).then((iduser)=>{
          //     this.http.put(this.URL+"Upinstlistuser?id="+x+"&token="+localStorage.getItem('token'),
          //         {idusers:iduser})
          //         .subscribe(data=>{
          //             console.log('is institute updated',data);
          //         })}
          // )

      })
    }
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PwcodevalidationPage } from './pwcodevalidation';

@NgModule({
  declarations: [
    PwcodevalidationPage,
  ],
  imports: [
    IonicPageModule.forChild(PwcodevalidationPage),
  ],
})
export class PwcodevalidationPageModule {}

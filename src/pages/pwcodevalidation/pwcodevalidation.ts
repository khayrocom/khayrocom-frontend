import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AlertController, IonicPage, LoadingController, NavController, NavParams,Platform} from 'ionic-angular';
import {ResetpasswordPage} from '../resetpassword/resetpassword'

import {Http} from "@angular/http";
import {env} from '../../environnement'
/**
 * Generated class for the PwcodevalidationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pwcodevalidation',
  templateUrl: 'pwcodevalidation.html',
})
export class PwcodevalidationPage {
	URL=env.URL;
  data: any =[];
    Email :any;
code: any;
verification: any =[];
  constructor(private LoadincC:LoadingController,
  private http:Http,
  public AlertC:AlertController,
  public navCtrl: NavController,
  public navParams: NavParams) {
  	this.Email=navParams.get('data');
  	  	console.log("this sheet",this.Email);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PwcodevalidationPage');
  }

  goBack(){
        this.navCtrl.pop();
    } 
passcode(){
      let loader = this.LoadincC.create({content:"Loading"});
      loader.present().then(()=>{
        this.http.post(this.URL+"midresetpw",{emailcode:this.code}).map(res => res.json())
  .subscribe(data=>{//function(data:response) {if (data.verification)
  	this.verification=data;
  	console.log('verif',data);
  	if (this.verification.verification=="true")
{this.navCtrl.push(ResetpasswordPage,{data:this.Email});}
 loader.dismissAll();

 },err=> {
let alert = this.AlertC.create(
              {
                title:"ERROR",
                message:err,
                buttons: [
                  {
                    text:"OK",
                    role:"cancel"

                  }
                ]
              }
            );
             }
)
    }
    )
	}
}
/* => {//data.forEach(x => this.usercompte.push(x));


            loader.dismissAll();
          },
          err => {
            let alert = this.AlertC.create(
              {
                title:"ERROR",
                message:err,
                buttons: [
                  {
                    text:"OK",
                    role:"cancel"

                  }
                ]
              }
            );
            alert.present();
          }
        )
      })*/

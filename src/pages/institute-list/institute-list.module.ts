import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InstituteListPage } from './institute-list';
import { SwipeSegmentDirective } from '../../directives/swipe-segment/swipe-segment';
import { DirectivesModule } from '../../directives/directives.module';


@NgModule({
  declarations: [
    InstituteListPage//,SwipeSegmentDirective
  ],
  imports: [
    IonicPageModule.forChild(InstituteListPage),DirectivesModule
  ]

})
export class InstituteListPageModule {}

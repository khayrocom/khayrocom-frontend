import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
// import {ApiHttpService} from '../Services/ApiHttpService.service';

import {AddInstitutePage} from "../add-institute/add-institute";
import {UpdateinstitutePage} from "../updateinstitute/updateinstitute";
import {ClassListPage} from "../class-list/class-list";
import {AlertController, IonicPage, LoadingController, NavController, NavParams,MenuController } from 'ionic-angular';
import {Http,Response} from "@angular/http";
import { Injectable } from '@angular/core';
import {env} from '../../environnement'

import {ReportPage} from "../report/report";
import {AuthProvider} from "../../providers/auth/auth";
import {HalaqaListPage} from "../halaqa-list/halaqa-list";


/**
 * Generated class for the InstituteListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  // name: 'Institutelist',
  // segment: 'Institutelist'
})
@Component({
  selector: 'page-institute-list',
  templateUrl: 'institute-list.html',
})
export class InstituteListPage {
URL=env.URL;
public isSearchbarOpened=false;
public ismenubarOpened=false;
  data: any =[];
   role:any;

  user:any;
  ListInstitutesNames : any [] = [];
  public category: string = 'LIST';
  public categories: Array<string> = ['LIST', 'INFO'];
  searchQuery:any;
    checkuserpermission:any;
    checkinstituteasadmin:any;
  constructor(private LoadincC:LoadingController,
    private http:Http,
    private AlertC:AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
  private menu: MenuController,
  public auth:AuthProvider) {
      this.auth.getrole.subscribe((role)=>this.role=role);
      //this.user = navParams.get('data');
      //localStorage.setItem("iduser",this.iduser);
      //this.doRefresh(0);
      //this.menu.enable(true,'first');
 //console.log("user ID",this.user.iduser);
  //this.getData();
  }


doRefresh(event){
  console.log("Refreshing...");
  setTimeout(() => {
    console.log('Async operation has ended');
    event.complete();
  }, 2000);}


  onSearch(ev) {
     this.http.get(this.URL+"institutesearchbyuser?q="+ev.target.value+"&iduser="+localStorage.getItem('iduser')).map(res => res.json())
     .subscribe(data =>
     {    let filtredlistinstitutename:any[]=[];

       data.forEach(x => {console.log("result",x);
       filtredlistinstitutename.unshift({InstituteName:x.InstituteName,idinstitute:x._id});

     });
     this.ListInstitutesNames=filtredlistinstitutename;

   })

  //  console.log(ev.target.value);
  }


  goBack(){
        this.navCtrl.pop();
    }

    ionViewWillEnter() {
        this.auth.checkuserpermission.subscribe((but)=>{this.checkuserpermission=but});

        this.getData();
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InstituteListPage');
  }

onTabChanged(tabName:any) {
    this.category = tabName;
  }
    GoToHalaqaInstitute(InstituteName: any,idinstitute:any) {
        console.log('go to class page', idinstitute);
        if(this.checkuserpermission=='true'|| this.role=='user'){
        this.navCtrl.push(HalaqaListPage,//ReportPage,//
            {data: {'InstituteName': InstituteName, 'idinstitute': idinstitute}});}
        else{
        this.navCtrl.push(ClassListPage,//ReportPage,//
            { data: {'InstituteName':InstituteName,'idinstitute':idinstitute}});
    }

  }

        goToAddInstitute(){
  	console.log('add institute page');
    this.navCtrl.push(AddInstitutePage,{data:localStorage.getItem('iduser')})
  }

  GoToClassInstitute(InstituteName: any,idinstitute:any){
  	console.log('go to class page',idinstitute);

  }

  getData(){
    console.log("url id user  ",this.URL+"institutebyuserid/"+localStorage.getItem('iduser'));
    console.log(this.URL+"institutebyuserid/"+localStorage.getItem('iduser'));

    if(this.role=='user'){
       this.http.get(this.URL+"institutebyuserid/"+localStorage.getItem('iduser')+"/"+localStorage.getItem('token'))
       .map(res => res.json())
       .subscribe(data =>
       {         this.ListInstitutesNames=[];

         data.forEach(x =>  this.ListInstitutesNames.unshift({InstituteName:x.InstituteName,idinstitute:x._id}));
         console.log("this.ListInstituesNames",this.ListInstitutesNames);
       console.log("****************"   ,data)},
          err => {
            let alert = this.AlertC.create(
              {
                title:"ERROR",
                message:err,
                buttons: [
                  {
                    text:"OK",
                    role:"cancel"

                  }
                ]
              }
            );
            alert.present();
          })}
    else if (this.checkuserpermission=='true')
    {
        this.http.get(this.URL+"institutebyuserid/"+localStorage.getItem('adminlookforuserpermission')+"/"+localStorage.getItem('token'))
            .map(res => res.json())
            .subscribe(data =>
                {         this.ListInstitutesNames=[];

                    data.forEach(x =>  this.ListInstitutesNames.unshift({InstituteName:x.InstituteName,idinstitute:x._id}));
                    console.log("this.ListInstituesNames",this.ListInstitutesNames);
                    console.log("****************"   ,data)},
                err => {
                    let alert = this.AlertC.create(
                        {
                            title:"ERROR",
                            message:err,
                            buttons: [
                                {
                                    text:"OK",
                                    role:"cancel"

                                }
                            ]
                        }
                    );
                    alert.present();
                });
    }
    // else if (this.checkinstituteasadmin=='true'){
    //     this.http.get(this.URL+"institutebyuserid/"+localStorage.getItem('adminlookforuserpermission')+"/"+localStorage.getItem('token'))
    //         .map(res => res.json())
    //         .subscribe(data =>
    //             {         this.ListInstitutesNames=[];
    //                 data.forEach(x =>  this.ListInstitutesNames.unshift({InstituteName:x.InstituteName,idinstitute:x._id}));
    //                 console.log("this.ListInstituesNames",this.ListInstitutesNames);
    //                 console.log("****************"   ,data)},
    //             err => {
    //                 let alert = this.AlertC.create(
    //                     {
    //                         title:"ERROR",
    //                         message:err,
    //                         buttons: [
    //                             {
    //                                 text:"OK",
    //                                 role:"cancel"
    //                             }
    //                         ]
    //                     }
    //                 );
    //                 alert.present();
    //             });
    //     this.auth.checkinstituteasadmin.next('false');
    // }
    else{

        this.http.get(this.URL+"institute/"+localStorage.getItem('token'))
            .map(res => res.json())
            .subscribe(data =>
                {         this.ListInstitutesNames=[];

                    data.forEach(x =>  this.ListInstitutesNames.unshift({InstituteName:x.InstituteName,idinstitute:x._id}));
                    console.log("this.ListInstituesNames",this.ListInstitutesNames);
                    console.log("****************"   ,data)},
                err => {
                    let alert = this.AlertC.create(
                        {
                            title:"ERROR",
                            message:err,
                            buttons: [
                                {
                                    text:"OK",
                                    role:"cancel"

                                }
                            ]
                        }
                    );
                    alert.present();
                })
    }

        //.map((res:Response ) =>  res.json())//.subscribe(data => { console.log("****************"   ,data);})
  }
  editItem(idinstitute:any){
    this.navCtrl.push(UpdateinstitutePage,
      { data: idinstitute});
  }
    removeItem(idinstitute:any){
        console.log('user will be deleted');
        let loader = this.LoadincC.create({ content: "deleting" ,duration: 1000});
        this.http.delete(this.URL+"institute?idinstitute="+idinstitute+"&token="+localStorage.getItem('token'))
            .subscribe(data=>{
                console.log('is institute deleted',data);
                loader.present();
                loader.dismissAll();
                this.ionViewWillEnter();
            },err => {
                let alert = this.AlertC.create(
                    {
                        title:"ERROR",
                        message:err,
                        buttons: [
                            {
                                text:"OK",
                                role:"cancel"

                            }
                        ]
                    }
                );
                alert.present();
            });

    }
}

import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {InstituteListPage} from "../institute-list/institute-list";
import {HomePage} from "../home/home";
import {LoginPage} from "../login/login";
import {RegistrationPage} from "../registration/registration";
import {ForgetpasswordPage} from "../forgetpassword/forgetpassword";
import {AlertController, IonicPage, LoadingController, NavController, NavParams,ActionSheetController } from 'ionic-angular';
import {Http} from "@angular/http";
import {env} from '../../environnement'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'

/**
 * Generated class for the LogoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
import { Injectable } from '@angular/core';
import {AuthProvider} from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-logout',
  templateUrl: 'logout.html',
})
@Injectable()
export class LogoutPage {
  role = '';
  URL=env.URL;
listtoken:any;
listuser:any [] = [];
listuserfinal:any [] = [];
localstoragelength;
  constructor(private LoadincC:LoadingController,
              private http:Http,private AlertC:AlertController,//public localStorage:IonicStorageModule,
               public navCtrl: NavController, public navParams: NavParams,
              public actionSheetCtrl: ActionSheetController,
              public auth:AuthProvider
  ) {
      // this.listtoken=localStorage.getItem('listtoken').split(",").filter(element => element !== "undefined");
      // console.log("newtokentolist array",this.listtoken) ;
  }

  presentActionSheet(token:any) {
    localStorage.setItem("token",token);

    const actionSheet = this.actionSheetCtrl.create({
    //  title: 'Modify your album',
      buttons: [
        {
          text: 'Delete',
          role: 'destructive',
          handler: () => {
            console.log('Destructive clicked');
            let tokenlist=localStorage.getItem("listtoken");
            let token = localStorage.getItem("token");
            localStorage.removeItem("token");
            tokenlist=tokenlist.replace(token,"undefined");
            localStorage.setItem("listtoken",tokenlist);
            console.log("delete token",tokenlist);
            this.ionViewWillEnter();
           }
        },

        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
      //ionViewWillEnter()
  }

deletecompte(){
  let tokenlist=localStorage.getItem("listtoken");
  let token = localStorage.getItem("token");
  tokenlist=tokenlist.replace(token,"undefined");
  localStorage.setItem("listtoken",tokenlist);
  //console.log("delete token",localstoragelength);
  //ionViewWillEnter();
}

  ionViewWillEnter() {
    console.log('ionViewDidLoad LogoutPage');
var uniqueuser = [];
let strtoken= 'undefined';
var itemsProcessed = 0;
this.listuserfinal.length=0;
  let localstoragelength = localStorage.getItem('listtoken').split(",").filter(element => element !== "undefined").length;
  if (localstoragelength==0){this.navCtrl.push(LoginPage)}
  console.log("ttttttttttttttttt",localstoragelength);
  console.log("localStorage.getItem('listtoken')",localStorage.getItem('listtoken').split(",").filter(element => element !== "undefined"));

  localStorage.getItem('listtoken').split(",").filter(element => element !== "undefined" ).forEach(x => {
    itemsProcessed++;
      this.http.get(this.URL+"verifytoken/"+x).map(res => res.json())
      .subscribe(dataverify =>{
      this.listuser.push({username:dataverify.username,email:dataverify.email,iduser:dataverify.iduser,role:dataverify.role,token:x});

      this.listuser.forEach(
        x=>{//console.log('element x',x.iduser);
        if(uniqueuser.indexOf(x.iduser)===-1){
          uniqueuser.push(x.iduser);
        this.listuserfinal.push(x);
        strtoken+=','+x.token;//+strtoken;

        }//{username:x.username,iduser:x.iduser,token:x.token}
      });
      // this.listuserfinal.forEach(x=>
      //   {
      //     strtoken= strtoken+','+x.token;
      //     //console.log('strtoken in forEach',strtoken);
      // })
     if (itemsProcessed  == localstoragelength)
     {
       localStorage.setItem("listtoken",strtoken);
       this.listuser.length = 0 ;
       console.log('strtoken in forEach',strtoken);
       console.log('       localStorage.setItem("listtoken",strtoken);',localStorage.getItem('listtoken').split(",").filter(element => element !== "undefined"));
       //console.log('strtoken in forEach',strtoken);

      // strtoken='undefined'
     }
    });
  });
  //this.listuser.length = 0 ;

  //this.listuser=this.listuserfinal;
console.log('listuserfinal in forEach',this.listuserfinal);

console.log('user list length in forEach',this.listuser.length);


console.log('user list by token **',this.listuser);
console.log('user list by token uniqueuser',uniqueuser);




  }
  gotoyourcount(token:any,iduser:any,role:any){
    console.log('gotoyourcount');
    console.log('gotoyourcount token:',token);
    console.log('gotoyourcount iduser:',iduser);
    localStorage.setItem('token', token);
    localStorage.setItem('role', role);
    localStorage.setItem('iduser', iduser);
    this.auth.getrole.next(localStorage.getItem("role"));
   this.navCtrl.push(HomePage,{data:iduser});
   //  this.navCtrl.setRoot(this.navCtrl.getActive().component,{data:iduser})
  }
gotologin(){
  localStorage.removeItem('token');
  this.navCtrl.push(LoginPage);
}
}

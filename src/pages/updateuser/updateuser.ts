import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {env} from '../../environnement';
import 'rxjs/add/operator/map';
import {AlertController, IonicPage, LoadingController, NavController, NavParams,MenuController } from 'ionic-angular';
import {Http,Response} from "@angular/http";

/**
 * Generated class for the UpdateuserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-updateuser',
  templateUrl: 'updateuser.html',
})
export class UpdateuserPage {
  URL=env.URL;
  iduser:any;
user:any;
  constructor(private LoadincC:LoadingController,
    private http:Http,
    private AlertC:AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
  private menu: MenuController) {
    this.iduser = navParams.get('data');

    this.http.get(this.URL+"gettoken?q="+this.iduser+"&token="+localStorage.getItem('iduser')).map(res => res.json())
    .subscribe(data =>
    {    this.user=data

    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdateuserPage');
  }

}

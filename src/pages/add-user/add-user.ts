import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController, IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';

import { Http } from "@angular/http";

import { UserListPage } from "../user-list/user-list";

import { env } from '../../environnement'

/**
 * Generated class for the AddUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-user',
  templateUrl: 'add-user.html',
})
export class AddUserPage {
  URL = env.URL;
//thisStatus=false;
  Class: any;
  StudentName: any;
  Gender: any;
  Status: any;
  RoomNumber: any;
  Level: any;
  EntryDate: any;
  ExitDate: any;
  constructor(private LoadincC: LoadingController, private http: Http, private AlertC: AlertController,
    public navCtrl: NavController, public navParams: NavParams) {
    this.Class = navParams.get('data');
    console.log("Institue name Nav params", this.Class);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddUserPage');
  }
  goBack() {
    this.navCtrl.pop();
  }
  goToUserList() {
    console.log('user  list page')
    this.navCtrl.push(UserListPage, { data: { 'ClassName': this.Class.ClassName, 'idclass': this.Class.idclass } });
  }
  save() {

    let loader = this.LoadincC.create({ content: "Loading" })
    loader.present().then(() => {
      this.http.post(this.URL + "student", {
StudentName: this.StudentName, Gender: this.Gender, Status: this.Status,
        RoomNumber: this.RoomNumber, Level: this.Level, EntryDate: this.EntryDate,
         ExitDate: this.ExitDate, idclass: this.Class.idclass,iduser:localStorage.getItem('iduser'), token: localStorage.getItem('token')
}).subscribe(
        data => {
          console.log(data);
          loader.dismissAll();
          this.navCtrl.pop();

        },
        err => {
          let alert = this.AlertC.create(
            {
              title: "ERROR",
              message: err,
              buttons: [
                {
                  text: "OK",
                  role: "cancel"
                }
              ]
            }
          );
          alert.present();
        }
      )
    })
  }
}

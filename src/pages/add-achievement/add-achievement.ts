import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';

import {Http} from "@angular/http";

import {AchievementListPage} from "../achievement-list/achievement-list";
import {env} from '../../environnement'


/**
 * Generated class for the AddAchievementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-achievement',
  templateUrl: 'add-achievement.html',
})
export class AddAchievementPage {
URL=env.URL;
Student:any;
  AchievDate :any;
  Program:any;
  Surah:any;
  FromVerse:any;
  ToVerse:any;
  Evaluation:any;
  Notes:any;
  constructor(private LoadincC:LoadingController,private http:Http,private AlertC:AlertController,
    public navCtrl: NavController, public navParams: NavParams){
      this.Student = navParams.get('data');
  console.log("Institue name Nav params",this.Student);
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddAchievementPage');
  }

goToAchievementList(){
  	console.log('achievement list page')
    this.navCtrl.push(AchievementListPage, { data: {'StudentName':this.Student.StudentName,'idstudent':this.Student.idstudent}});
  }
  goBack(){
      this.navCtrl.pop();
  }
save(){
if(this.AchievDate && this.Surah && this.Program && this.FromVerse && this.ToVerse && this.Evaluation && this.Notes){
      let loader = this.LoadincC.create({content:"Loading"})
      loader.present().then(()=>{
        this.http.post(this.URL+"achievement",{AchievDate:this.AchievDate,Program:this.Program,Surah:this.Surah,
        FromVerse:this.FromVerse,ToVerse:this.ToVerse,Evaluation:this.Evaluation,Notes:this.Notes,idstudent:this.Student.idstudent,token:localStorage.getItem('token')}).subscribe(
          data  =>{
            console.log(data);
            loader.dismissAll();
            this.navCtrl.pop();

          },
          err => {
            let alert = this.AlertC.create(
              {
                title:"ERROR",
                message:err,
                buttons: [
                  {
                    text:"OK",
                    role:"cancel"

                  }
                ]
              }
            );
            alert.present();
          }
        )
      })
    }
else{

    let alert = this.AlertC.create(
      {
        title:"ERROR",
        message:'one or multiple fields are empty',
        buttons: [
          {
            text:"OK",
            role:"cancel"

          }
        ]
      }
    );
    alert.present();


}
  }
}

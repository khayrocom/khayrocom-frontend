import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddAchievementPage } from './add-achievement';
//
@NgModule({
  declarations: [
    AddAchievementPage,
  ],
  imports: [
    IonicPageModule.forChild(AddAchievementPage),
  ],
})
export class AddAchievementPageModule {}

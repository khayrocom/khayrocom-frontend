import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {InstituteListPage} from "../institute-list/institute-list";
import {HomePage} from "../home/home";
import {RegistrationPage} from "../registration/registration";
import {LogoutPage} from "../logout/logout";
import {ForgetpasswordPage} from "../forgetpassword/forgetpassword";
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Http} from "@angular/http";
import {env} from '../../environnement'

//import {IonicStorageModule} from '@ionic/Storage';


import { AuthProvider } from '../../providers/auth/auth';
import CryptoJS from 'crypto-js';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
    data1: any;
  Email :any;
  Password:any;
  data: any =[];
URL=env.URL;
  constructor(private LoadincC:LoadingController,private http:Http,private AlertC:AlertController,//public localStorage:IonicStorageModule,
    public navCtrl: NavController, public navParams: NavParams, public AuthProvider: AuthProvider,
              public auth:AuthProvider) {




      // else {
      //   let rootPageParams = {'idstudent': data.idstudent };
      //   this.khayrocom.getRootNav().setRoot(InstituteListPage, rootPageParams);
      //
      // //  rootPage:any = LoginPage;
      // }
  }


  ionViewWillEnter() {
    console.log('ionViewDidLoad LoginPage');
    this.http.get(this.URL+"verifytoken/"+localStorage.getItem('token')).map(res => res.json())
    .subscribe(dataverify =>{
      console.log('before if ,token verifytoken',dataverify);

    if(dataverify.success==true){
      localStorage.setItem("iduser",dataverify.iduser);
      localStorage.setItem("role",dataverify.role);
      this.auth.getrole.next(dataverify.role);

    //  console.log('token verifytoken ',dataverify);
    this.navCtrl.push(HomePage,{iduser:dataverify.iduser,role:dataverify.role});
    }
    else{
    localStorage.removeItem('token');
    //      localStorage.reset();
    }
    })


  }

  Check(){
    let loader = this.LoadincC.create({content:"Loading",duration: 1000});
    let hashpassword = CryptoJS.SHA256(this.Password).toString(CryptoJS.enc.Hex);
    var info={'name': this.Email,'password':hashpassword};
    console.log("email password"   ,info);

    //this.AuthProvider.login(info).subscribe(result => {
      this.http.post(this.URL+"login",{email:this.Email.replace(/\s/g,'').toLowerCase(),password:hashpassword}).subscribe(result => {
      this.data1 = result;

      localStorage.setItem('token', this.data1.json().token);
      let newtokentolist=localStorage.getItem("listtoken");

      newtokentolist=localStorage.getItem('token') +","+newtokentolist;
      localStorage.setItem("listtoken",newtokentolist);

 console.log("role======"   ,this.data1.json().role);

            localStorage.setItem("role",this.data1.json().role);

  loader.present();
  console.log("result.iduser push login", this.data1.json().iduser);
  localStorage.setItem("iduser",this.data1.json().iduser);
  this.auth.getrole.next(this.data1.json().role);
    this.navCtrl.push(HomePage ,{data:result.json().iduser});
              loader.dismissAll();
          },
          err => {
            let alert = this.AlertC.create(
              {
                title:"ERROR",
                message:err,
                buttons: [
                  {
                    text:"OK",
                    role:"cancel",
                    handler: ()=>{
                        console.log("hachemmm");
                      loader.dismissAll();

                    }
                  }
                ]

          }

            );
            alert.present();
          }

      )
      }


  goToRegister(){

    this.navCtrl.push(RegistrationPage);
  }
  goToForget(){
    this.navCtrl.push(ForgetpasswordPage);
  }



}

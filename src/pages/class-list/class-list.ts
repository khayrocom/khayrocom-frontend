import {Component} from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Http, Response} from "@angular/http";
import {AddClassPage} from "../add-class/add-class";
import {UserListPage} from "../user-list/user-list";
import {UpdateclassPage} from "../updateclass/updateclass";

import 'rxjs/add/operator/map';

import {env} from '../../environnement'
import {StudentListPage} from "../student-list/student-list";
import {AuthProvider} from "../../providers/auth/auth";


/**
 * Generated class for the ClassListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-class-list',
    templateUrl: 'class-list.html',
})
export class ClassListPage {
    data: any = [];
    ListClassNames: any [] = [];
    Institute: any;
    URL = env.URL;
    role1:any;

    public isSearchbarOpened = false;

    public category: string = 'LIST';
    public categories: Array<string> = ['LIST', 'INFO']

    constructor(private LoadincC: LoadingController,
                private http: Http,
                private AlertC: AlertController,
                public navCtrl: NavController,
                public navParams: NavParams,
                public auth:AuthProvider) {
        this.Institute = navParams.get('data');

        console.log("class recived Nav params", this.Institute);
        localStorage.setItem("Institute", this.Institute.idinstitute);
        this.auth.getrole.subscribe((role)=>this.role1=role);


    }

    goBack() {
        this.navCtrl.pop();
    }

    ionViewWillEnter() {
        this.auth.checkuserpermission.next('false');

//this.ListInstituesNames = [];
        this.getclass();
    }

    onSearch(ev) {
        this.http.get(this.URL + "classsearchbyinstitute?q=" + ev.target.value + "&idinstitute=" + this.Institute.idinstitute).map(res => res.json())
            .subscribe(data => {
                let filtredlistname: any[] = [];

                data.forEach(x => {
                    console.log("result", x);
                    filtredlistname.unshift({ClassName: x.ClassName, idclass: x._id});

                });
                this.ListClassNames = filtredlistname;

            })

        //  console.log(ev.target.value);
    }


    onTabChanged(tabName: any) {
        this.category = tabName;
    }

    goToAddClass() {
        console.log('add class page');
        this.navCtrl.push(AddClassPage, {
            data: {
                'InstituteName': this.Institute.InstituteName,
                'idinstitute': this.Institute.idinstitute
            }
        });
    }

    GoToClassStudent(ClassName: any, _id: any) {
        console.log('add institute page');
        localStorage.setItem('idclass',_id);

        this.navCtrl.push(StudentListPage,
            {data: {'ClassName': ClassName, 'idclass': _id}});
    }

    getclass() {
        console.log(this.URL + "classbyinstituteid/" + this.Institute.idinstitute);
        this.http.get(this.URL + "classbyinstituteid/" + localStorage.getItem('Institute') + "/" + localStorage.getItem('token'))
            .map(res => res.json())
            .subscribe(data => {
                    this.ListClassNames = [];
                    data.forEach(x => this.ListClassNames.unshift({ClassName: x.ClassName, idclass: x._id}));

                    console.log("this.ListClassNames", this.ListClassNames);
                    console.log("****************", data)
                },
                err => {
                    let alert = this.AlertC.create(
                        {
                            title: "ERROR",
                            message: err,
                            buttons: [
                                {
                                    text: "OK",
                                    role: "cancel"

                                }
                            ]
                        }
                    );
                    alert.present();
                })

        //.map((res:Response ) =>  res.json())//.subscribe(data => { console.log("****************"   ,data);})
    }

    editItem(idclass: any) {
        this.navCtrl.push(UpdateclassPage,
            {data: idclass});
    }
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClassListPage } from './class-list';
import { SwipeSegmentDirective } from '../../directives/swipe-segment/swipe-segment';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    ClassListPage//,SwipeSegmentDirective
  ],
  imports: [
    IonicPageModule.forChild(ClassListPage),DirectivesModule
  ]
})
export class ClassListPageModule {}

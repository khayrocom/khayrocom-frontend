import { Component } from '@angular/core';
import {env} from '../../environnement'
import 'rxjs/add/operator/map';
import {AlertController, IonicPage, LoadingController, NavController, NavParams,MenuController } from 'ionic-angular';
import {Http,Response} from "@angular/http";
import {UserListPage} from "../user-list/user-list";

/**
 * Generated class for the UpdatestudentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-updatestudent',
  templateUrl: 'updatestudent.html',
})
export class UpdatestudentPage {
  URL=env.URL;
  idstudent:any;
student:any[]=[];
  constructor(private LoadincC:LoadingController,
    private http:Http,
    private AlertC:AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
  private menu: MenuController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdatestudentPage');
    this.idstudent = this.navParams.get('data');

    this.http.get(this.URL+"studentinfo?id="+this.idstudent+"&token="+localStorage.getItem('token'))
    .map(res => res.json())
    .subscribe(data =>{
      this.student=data;
console.log('update',data);
    });

}
updatestudent(idstudent:any){

  const confirm = this.AlertC.create({
   title: 'Confirmation Alert',
   message: 'do you really want to change the information about this student?',
   buttons: [
     {
       text: 'cancel',
       handler: () => {
         console.log('Disagree clicked');

       }
     },
     {
       text: 'Agree',
       handler: () => {
         console.log('Agree clicked');
       this.http.put(this.URL+"stupdate?id="+idstudent+"&token="+localStorage.getItem('token'),this.student)
       .subscribe(data=>{
         console.log('is student updated',data);
       })
     console.log('student updated=',this.student);

       this.navCtrl.pop();

     }
   }
 ]
});
confirm.present();


}

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdatestudentPage } from './updatestudent';

@NgModule({
  declarations: [
    UpdatestudentPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdatestudentPage),
  ],
})
export class UpdatestudentPageModule {}


import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';

import {Http} from "@angular/http";
import {AddUserPage} from "../add-user/add-user";
import {AchievementListPage} from "../achievement-list/achievement-list";
import {UpdatestudentPage} from "../updatestudent/updatestudent";

import 'rxjs/add/operator/map';
import {env} from '../../environnement'
import {AddStudentPage} from "../add-student/add-student";
import {AuthProvider} from "../../providers/auth/auth";

/**
 * Generated class for the StudentListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-student-list',
  templateUrl: 'student-list.html',
})
export class StudentListPage {
 URL=env.URL;
  public isSearchbarOpened=false;
    Classes='all';
  data: any =[];
  ListStudentNames : any [] = [];
  Class:any;
Halaqa:any;
    checkinstituteasadmin:any;
role:any;
  public category: string = 'Patient';
  public categories: Array<string> = ['Patient', 'Employee'];
    ListClass:any [] = [];
  constructor(private LoadincC:LoadingController,
              private http:Http,
              private AlertC:AlertController,
              public navCtrl: NavController,
              public navParams: NavParams,
              public auth:AuthProvider) {
    this.Halaqa = navParams.get('data');
    console.log("class name and id Nav params",this.Halaqa);
      this.http.get(this.URL + "classbyinstituteid/" + localStorage.getItem('Institute')
          + "/" + localStorage.getItem('token'))
          .map(res => res.json())
          .subscribe(data => {
                  this.ListClass = [];
                  data.forEach(x => this.ListClass.unshift({ClassName: x.ClassName, idclass: x._id}));

                  console.log("this.ListClassNames", this.ListClass);
                  console.log("****************", data)
              },
              err => {
                  let alert = this.AlertC.create(
                      {
                          title: "ERROR",
                          message: err,
                          buttons: [
                              {
                                  text: "OK",
                                  role: "cancel"

                              }
                          ]
                      }
                  );
                  alert.present();
              })
  }
  segmentChanged(ev: any) {
    console.log('Segment changed', ev);
  }
  goBack(){
    this.navCtrl.pop();
  }


  ionViewWillEnter() {
      this.auth.checkuserpermission.subscribe((but)=>{this.checkinstituteasadmin=but});
      this.auth.getrole.subscribe((role)=>this.role=role);

    //this.ListInstituesNames = [];
    this.getstudent();

  }

  onSearch(ev) {
      if (this.Classes == 'all') {
          this.http.get(this.URL + "studentsearchbyhalaqa?q=" + ev.target.value
              + "&idhalaqa=" + localStorage.getItem('idhalaqa'))
              .map(res => res.json())
              .subscribe(data => {
                  let filtredlistname: any[] = [];

                  data.forEach(x => {
                      console.log("result", x);

                     if(x.archive=='false') {
                         filtredlistname.unshift({StudentName: x.StudentName, idstudent: x._id, Status: x.Status});
                     }
                  });
                  this.ListStudentNames = filtredlistname;

              })

          //  console.log(ev.target.value);
      }
      else{
          this.http.get(this.URL + "studentsearchbyhalaqaandclass?q=" + ev.target.value
              + "&idhalaqa=" + localStorage.getItem('idhalaqa')
              + "&idclass=" +this.Classes)
              .map(res => res.json())
              .subscribe(data => {
                  let filtredlistname: any[] = [];

                  data.forEach(x => {
                      console.log("result", x);
                      if(x.archive=='false') {

                          filtredlistname.unshift({StudentName: x.StudentName, idstudent: x._id, Status: x.Status});
                      }
                  });
                  this.ListStudentNames = filtredlistname;

              })


      }
  }

  onTabChanged(tabName:any) {
    this.category = tabName;
  }
  goToAddUser(){
    console.log('add user page')
    this.navCtrl.push(AddStudentPage,  { data: {'HalaqaName':this.Halaqa.HalaqaName,
            'idhalaqa':localStorage.getItem('idhalaqa')}});
  }
  GoToAchievment(StudentName:any,idstudent:any){
    this.navCtrl.push(AchievementListPage,
        { data: {'StudentName':StudentName,'idstudent':idstudent}});
  }
  getstudent(){
    console.log(this.URL+"studentbyhalaqaid/"+localStorage.getItem('idhalaqa')+"/"+localStorage.getItem('token'));
    if (this.Classes=='all' && this.checkinstituteasadmin=='false') {
        this.http.get(this.URL + "studentbyhalaqaid/" + localStorage.getItem('idhalaqa')
            + "/" + localStorage.getItem('token'))
            .map(res => res.json())
            .subscribe(data => {
                    this.ListStudentNames = [];
                    data.forEach(x => {
                        if (x.archive == 'false') {
                            this.ListStudentNames.unshift({StudentName: x.StudentName, idstudent: x._id, Status: x.Status})
                        }
                    });

                    console.log("this.ListStudentNames", this.ListStudentNames);
                    console.log("****************", data)
                },
                err => {
                    let alert = this.AlertC.create(
                        {
                            title: "ERROR",
                            message: err,
                            buttons: [
                                {
                                    text: "OK",
                                    role: "cancel"

                                }
                            ]
                        }
                    );
                    alert.present();
                })
    }
    else if(this.checkinstituteasadmin=='true'){
        this.http.get(this.URL + "studentbyclassid/" + localStorage.getItem('idclass')
            + "/" + localStorage.getItem('token'))
            .map(res => res.json())
            .subscribe(data => {
                    this.ListStudentNames = [];
                    data.forEach(x => {
                        if (x.archive == 'false') {
                            this.ListStudentNames.unshift({StudentName: x.StudentName, idstudent: x._id, Status: x.Status})
                        }
                    });

                    console.log("this.ListStudentNames", this.ListStudentNames);
                    console.log("****************", data)
                },
                err => {
                    let alert = this.AlertC.create(
                        {
                            title: "ERROR",
                            message: err,
                            buttons: [
                                {
                                    text: "OK",
                                    role: "cancel"

                                }
                            ]
                        }
                    );
                    alert.present();
                })
    }
    else{
        this.http.get(this.URL + "studentdoublefilter/" +
            localStorage.getItem('idhalaqa') + "/" + this.Classes+
            "/" + localStorage.getItem('token'))
            .map(res => res.json())
            .subscribe(data => {
                    this.ListStudentNames = [];
                    data.forEach(x => {
                        if (x.archive == 'false') {
                            this.ListStudentNames.unshift({StudentName: x.StudentName, idstudent: x._id, Status: x.Status})
                        }
                    });

                    console.log("this.ListStudentNames", this.ListStudentNames);
                    console.log("****************", data)
                },
                err => {
                    let alert = this.AlertC.create(
                        {
                            title: "ERROR",
                            message: err,
                            buttons: [
                                {
                                    text: "OK",
                                    role: "cancel"

                                }
                            ]
                        }
                    );
                    alert.present();
                })
    }
    //.map((res:Response ) =>  res.json())//.subscribe(data => { console.log("****************"   ,data);})
  }


  archiveItem(idstudent:any){
    this.http.put(this.URL+"stupdate?id="+idstudent+"&token="+localStorage.getItem('token'),{archive:"true"}).subscribe(data=>{
      console.log('is student updated archive',data);
      this.getstudent();
    })

  }

  editItem(idstudent:any){
    this.navCtrl.push(UpdatestudentPage,
        { data: idstudent});

  }
}


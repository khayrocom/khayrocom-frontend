import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountInfoEditPage } from './account-info-edit';

@NgModule({
  declarations: [
    AccountInfoEditPage,
  ],
  imports: [
    IonicPageModule.forChild(AccountInfoEditPage),
  ],
})
export class AccountInfoEditPageModule {}

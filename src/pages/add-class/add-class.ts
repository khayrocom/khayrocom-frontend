import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';

import {Http} from "@angular/http";
import {ClassListPage} from "../class-list/class-list";
import {env} from '../../environnement'


/**
 * Generated class for the AddClassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-class',
  templateUrl: 'add-class.html',
})
export class AddClassPage {

  InstituteName: any;
  Institute: any;
URL=env.URL;
  ClassName:any;
  constructor(private LoadincC:LoadingController,private http:Http,
    private AlertC:AlertController,public navCtrl: NavController,
     public navParams: NavParams) {
       this.Institute = navParams.get('data');
  console.log("Institute name Nav params",this.Institute);
       }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddClassPage');
  }

  goToClassList(){
  	console.log('class list page')
    this.navCtrl.push(ClassListPage,{ data: {'InstituteName':this.Institute.InstituteName,'idinstitute':this.Institute.idinstitute}});
  }
  goBack(){
        this.navCtrl.pop();
    }
AddClass(){

      let loader = this.LoadincC.create({content:"Loading"})
      loader.present().then(()=>{
        this.http.post(this.URL+"class",{ClassName:this.ClassName,idinstitute:this.Institute.idinstitute,iduser:localStorage.getItem('iduser'),token:localStorage.getItem('token')}).subscribe(
          data  =>{
            console.log(data);
            loader.dismissAll();
            this.navCtrl.pop();

          },
          err => {
            let alert = this.AlertC.create(
              {
                title:"ERROR",
                message:err,
                buttons: [
                  {
                    text:"OK",
                    role:"cancel"

                  }
                ]
              }
            );
            alert.present();
          }
        )
      })
    }
}

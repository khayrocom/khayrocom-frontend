import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams,Platform} from 'ionic-angular';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import {Http} from "@angular/http";

import {env} from '../../environnement'
//var async = require("async");

/**
 * Generated class for the ReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-report',
  templateUrl: 'report.html',
})
export class ReportPage {
  Notes:any;
  FromDate :any;
  ToDate :any;
  content1:any[]=[];

URL=env.URL;
ListClassNames : any [] = [];
ListClass : any [] = [];
ListAchievement : any [] = [];
ListStudentNames : any [] = [];
ListStudent : any [] = [];
Institute: any;
Class: any;
   Student : any;
    pdfObj = null;
 constructor(private LoadincC:LoadingController,
   private http:Http,
   private AlertC:AlertController,
   public navCtrl: NavController,
   public navParams: NavParams, private plt: Platform, private file: File, private fileOpener: FileOpener){

         this.Institute = navParams.get('data');
    console.log("Institute name and id class list Nav params",this.Institute);
  //  this.getclass();

  }

  goBack(){
      this.navCtrl.pop();
  }
  getachievement(idstudent:any){
  this.http.get(this.URL+"achievementbystudentid/"+idstudent+"/"+localStorage.getItem('token'))
  .map(res => res.json())
  .subscribe(data =>
  {
   data.forEach(x => this.ListAchievement.push(x));
   console.log("3");

   console.log("this.ListAchievement",this.ListAchievement);
  console.log("****************"   ,data)}
  ,
    err => {
      let alert = this.AlertC.create(
        {
          title:"ERROR",
          message:err,
          buttons: [
            {
              text:"OK",
              role:"cancel"

            }
          ]
        }
      );
      alert.present();
    })
}

   getstudent(idclass:any){
        console.log(this.URL+"studentbyclassid/"+idclass);
        this.http.get(this.URL+"studentbyclassid/"+idclass+"/"+localStorage.getItem('token'))
       .map(res => res.json())
       .subscribe(data =>
       {let i=0;
         data.forEach(x => {
           i++;
           // this.ListAchievement.length=0;
           this.ListStudent.push(x);
           this.ListStudentNames.push(x.StudentName);
           this.getachievement(x._id);
         } );//{StudentName:x.StudentName,_id:x._id}
         console.log("2");

         console.log('iiiiiiiiiiiiiiiiiiiiiiii',i);
         console.log('iiiiiiiiiidata.lengthiiiiiiiiiiiiii',data.length);

if(i===data.length){
  console.log('this.ListStudentNames §!!!!!!!!!!!!!!!!',this.ListStudentNames)

  // this.content1.push({
  //            // style: 'itemsTable',
  //            table: {
  //  //  widths: [200, 'auto', 'auto'],
  //  //  headerRows: 2,
  //  // keepWithHeaderRows: 1,
  //  body: [
  //    //[{text: 'Header with Colspan = 2', style: 'tableHeader', colSpan: 2, alignment: 'center'}, {}, {text: 'Header 3', style: 'tableHeader', alignment: 'center'}],
  //    //[{text: 'Header 1', style: 'tableHeader', alignment: 'center'}, {text: 'Header 2', style: 'tableHeader', alignment: 'center'}, {text: 'Header 3', style: 'tableHeader', alignment: 'center'}],
  //    [{rowSpan:2,fillColor: '#cccccc',text:'Dates'},{fillColor: '#cccccc',text:'from'},
  //    {colSpan:3,text:this.FromDate},'','', {fillColor: '#cccccc',rowSpan:2,text:'Month'},{rowSpan:2,text:'Dates'}],
  //
  //    ['',{fillColor: '#cccccc',text:'to'}, {colSpan:3,text:this.ToDate},'','', '',''],
  //    [{rowSpan:2,fillColor: '#cccccc',text:'Memorize and improvement'},{rowSpan:2,fillColor: '#cccccc',text:'Memorized'},
  //     {text:'pages'},{text:'Surahs'},{text:'verses'}, {fillColor: '#cccccc',text:'Students motivation'},{  text:'Satisfactory'}],
  //
  //     ['','',{text:'50'},{text:'30'},{text:'500'}, {fillColor: '#cccccc',text:'Students level'},{text:'good'}],
  //    [{colSpan:2,fillColor: '#cccccc',text:'nember of student'},'', {colSpan:3,text:this.ListStudentNames.length},'','',
  //    {fillColor: '#cccccc',text:'class'},{text:'ClassName'}],
  //    [{colSpan:2,fillColor: '#cccccc',text: 'Student names'},'',{colSpan: 5, table: {body:[].concat(this.listToMatrix(this.ListStudentNames,3))}},'','','',''],
  //    [{colSpan:2,fillColor: '#cccccc',text:'Notes'},'',{colSpan:5,text:this.Notes},'','','','']
  //  ]
  //  }
  //  });

         console.log("this.liststuedent report",this.ListStudentNames);
       console.log("****************"   ,data);
      }

    },
          err => {
            let alert = this.AlertC.create(
              {
                title:"ERROR",
                message:err,
                buttons: [
                  {
                    text:"OK",
                    role:"cancel"

                  }
                ]
              }
            );
            alert.present();
          })

        //.map((res:Response ) =>  res.json())//.subscribe(data => { console.log("****************"   ,data);})
  };
       getclass(idinstitute:any){
        this.http.get(this.URL+"classbyinstituteid/"+idinstitute+"/"+localStorage.getItem('token'))
      //.toPromise()
       .map(res => res.json())
       .subscribe(data =>
       {
          for (let x of data) {
           let promises = [];
         //data.forEach(x => {
           console.log("1");
           // this.ListStudentNames.length=0;
           // this.ListStudent.length=0;
           // this.ListAchievement.length=0;
           this.ListClassNames.push(x.ClassName);
         this.ListClass.push(x);

 this.getstudent(x._id);
//  Promise.all();



             //console.log("3");

};

         console.log("this.ListClassNames report",this.ListClassNames);
       console.log("****************"   ,data)},
          err => {
            let alert = this.AlertC.create(
              {
                title:"ERROR",
                message:err,
                buttons: [
                  {
                    text:"OK",
                    role:"cancel"

                  }
                ]
              }
            );
            alert.present();
          })
  }




  listToMatrix(list, elementsPerSubArray) {
     var matrix = [], i, k;
     for (i = 0, k = -1; i < list.length; i++) {
         if (i % elementsPerSubArray === 0) {
             k++;
             matrix[k] = [];
         }
         matrix[k].push(list[i]);
     }
     console.log('i lit to matrix',i);
    let r=i%elementsPerSubArray;
    if(r!=0){
     for (let l = 0; l < elementsPerSubArray-r; l++) {
         matrix[k].push('----');
     }}
     return matrix;
 }



createPdf() {
//  this.ListAchievement.
  var items = this.ListAchievement.map(function(item) {
  //  if item!=
//  console.log(item);
       return [item.AchievDate ?item.AchievDate:"N/A",
        item.Program?item.Program:"N/A",
       item.Surah?item.Surah:"N/A",
       item.FromVerse?item.FromVerse:"N/A",
        item.ToVerse?item.ToVerse:"N/A",
        item.Evaluation?item.Evaluation:"N/A",
         item.Notes?item.Notes:"N/A"];
   });

            // var docDefinition = {content:{
            //     style: 'itemsTable',
            //     table: {
            //         //widths: ['*', 75, 75],
            //         body: [
            //             [
            //                 { text: 'AchievDate', style: 'itemsTableHeader' },
            //                 { text: 'Program', style: 'itemsTableHeader' },
            //                 { text: 'Surah', Surah: 'itemsTableHeader' },
            //                 { text: 'FromVerse', Surah: 'itemsTableHeader' },
            //                 { text: 'ToVerse', Surah: 'itemsTableHeader' },
            //                 { text: 'Evaluation', Surah: 'itemsTableHeader' },
            //                 { text: 'Notes', Surah: 'itemsTableHeader' }
            //                ]
            //         ].concat(items)
            //     }}}

      console.log('this.ListStudentNames.length==',this.ListStudentNames.length);
      this.content1.push({
              style: 'itemsTable',
              table: {
  //  widths: ['auto', 'auto','auto', 'auto','auto',  'auto', 'auto'],
  //  headerRows: 2,
    // keepWithHeaderRows: 1,
    body: [
      //[{text: 'Header with Colspan = 2', style: 'tableHeader', colSpan: 2, alignment: 'center'}, {}, {text: 'Header 3', style: 'tableHeader', alignment: 'center'}],
      //[{text: 'Header 1', style: 'tableHeader', alignment: 'center'}, {text: 'Header 2', style: 'tableHeader', alignment: 'center'}, {text: 'Header 3', style: 'tableHeader', alignment: 'center'}],
      [{rowSpan:2,fillColor: '#cccccc',text:'Dates'},{fillColor: '#cccccc',text:'from'},
      {colSpan:3,text:this.FromDate},'','', {fillColor: '#cccccc',rowSpan:2,text:'Month'},{rowSpan:2,text:'Dates'}],

      ['',{fillColor: '#cccccc',text:'to'}, {colSpan:3,text:this.ToDate},'','', '',''],
      [{rowSpan:2,fillColor: '#cccccc',text:'Memorize and improvement'},{rowSpan:2,fillColor: '#cccccc',text:'Memorized'},
       {text:'pages'},{text:'Surahs'},{text:'verses'}, {fillColor: '#cccccc',text:'Students motivation'},{  text:'Satisfactory'}],

       ['','',{text:'50'},{text:'30'},{text:'500'}, {fillColor: '#cccccc',text:'Students level'},{text:'good'}],
      [{colSpan:2,fillColor: '#cccccc',text:'nember of student'},'', {colSpan:3,text:this.ListStudentNames.length},'','',
      {fillColor: '#cccccc',text:'class'},{text:'ClassName'}],
      [{colSpan:2,fillColor: '#cccccc',text: 'Student names'},'',{colSpan: 5, table: {body:[].concat(this.listToMatrix(this.ListStudentNames,3))}},'','','',''],
      [{colSpan:2,fillColor: '#cccccc',text:'Notes'},'',{colSpan:5,text:this.Notes},'','','','']
    ]
  }
});
      var docDefinition = {
        content:this.content1
      };
  //{text: 'Headers', pageBreak: 'before'}

  //  content:content1;

    //}
    console.log('this.content1==',this.content1);

  this.pdfObj = pdfMake.createPdf(docDefinition);
  if (this.plt.is('cordova')) {
     this.pdfObj.getBuffer((buffer) => {
       var blob = new Blob([buffer], { type: 'application/pdf' });

       // Save the PDF to the data Directory of our App
       this.file.writeFile(this.file.dataDirectory, 'achievement.pdf', blob, { replace: true }).then(fileEntry => {
         // Open the PDf with the correct OS tools
         this.fileOpener.open(this.file.dataDirectory + 'achievement.pdf', 'application/pdf');
       })
     });
   } else {
     // On a browser simply use download!
     this.pdfObj.download();
   }
 }

}

import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {AchievementListPage} from "../achievement-list/achievement-list";
import {UserListPage} from "../user-list/user-list";
import {ClassListPage} from "../class-list/class-list";
import {env} from '../../environnement'
import {Http} from "@angular/http";
import 'rxjs/add/operator/map';
import {AuthProvider} from "../../providers/auth/auth";
import {HalaqaListPage} from "../halaqa-list/halaqa-list";
import {StudentListPage} from "../student-list/student-list";

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  URL=env.URL;
ListInstitutes:any[]=[];
ListClass:any[]=[];
    ListStudent:any[]=[];
    ListHalaqa:any[]=[];
  constructor(private LoadincC:LoadingController,
    private http:Http,
    private AlertC:AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
  public auth:AuthProvider) {

      this.auth.getrole.next(localStorage.getItem("role"));

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }
  goBack(){
        this.navCtrl.pop();
    }
    onSearch(ev) {
      if(ev.target.value=='')
{ this.ListStudent.length=0;
 this.ListInstitutes.length=0;
    this.ListHalaqa.length=0;
        this.ListClass.length=0;}
 else{
       this.http.get(this.URL+"globsearch?q="+ev.target.value+"&iduser="+localStorage.getItem('iduser'))
           .map(res => res.json())
       .subscribe(data =>
       {
         let  searchedliststudent:any[]=[];
         let searchedlistinstitutename:any[]=[];
           let searchedlistclass:any[]=[];
           let searchedlisthalaqa:any[]=[];
console.log('dataglob',data);
         if(data[0]!=[]){

         data[0].forEach(x => {//console.log("result",x);
         searchedliststudent.push({StudentName:x.StudentName,idstudent:x._id});

       });
     }
     if(data[1]!=[]){

       data[1].forEach(x => {//console.log("result",x);
       searchedlistinstitutename.push({InstituteName:x.InstituteName,idinstitute:x._id});

     });
   }
   if(data[2]!=[]) {

       data[2].forEach(x => {//console.log("result",x);
           searchedlistclass.push({ClassName: x.ClassName, idclass: x._id});

       });
   }
       if(data[3]!=[]){

           data[3].forEach(x => {//console.log("result",x);
               searchedlisthalaqa.push({HalaqaName:x.HalaqaName,idhalaqa:x._id});

           });
 }
 this.ListStudent=searchedliststudent;
 this.ListInstitutes=searchedlistinstitutename;
 this.ListClass=searchedlistclass;
 this.ListHalaqa=searchedlisthalaqa;
 console.log(this.ListStudent)

     })
     }
     console.log('ev.target.value',ev.target.value);
    }

    GoToClassInstitue(InstituteName: any,idinstitute:any){
    	console.log('go to class page',idinstitute)
      this.navCtrl.push(HalaqaListPage,//ReportPage,//
         { data: {'InstituteName':InstituteName,'idinstitute':idinstitute}});
    }
    GoToClassStudent(ClassName: any,_id:any){
    	console.log('add institute page');
      this.navCtrl.push(StudentListPage,
        { data: {'ClassName':ClassName,'idclass':_id}});
    }
    GoToAchievment(StudentName:any,idstudent:any){
      this.navCtrl.push(AchievementListPage,
        { data: {'StudentName':StudentName,'idstudent':idstudent}});
    }
    GoToHalaqaClasses(HalaqaName:any,idhalaqa:any){
        this.navCtrl.push(ClassListPage,
            { data: {'HalaqaName':HalaqaName,'idhalaqa':idhalaqa}});
    }
}

import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AlertController, IonicPage, LoadingController, NavController, NavParams,Platform} from 'ionic-angular';

import {Http} from "@angular/http";

import {AddAchievementPage} from "../add-achievement/add-achievement";

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import {env} from '../../environnement'

/**
 * Generated class for the AchievementListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-achievement-list',
  templateUrl: 'achievement-list.html',
})
export class AchievementListPage {
URL=env.URL;
  data: any =[];
  ListAchievement : any [] = [];
Student:any;
pdfObj = null;

constructor(private LoadincC:LoadingController,
  private http:Http,
  private AlertC:AlertController,
  public navCtrl: NavController,
  public navParams: NavParams,
 private plt: Platform, private file: File, private fileOpener: FileOpener) {
        this.Student = navParams.get('data');
    console.log("student name Nav params",this.Student);
    localStorage.setItem("idstudent",this.Student.idstudent);
    console.log("idstudent will be searched",localStorage.getItem('idstudent'));


    }

    ionViewWillEnter() {

   //this.ListInstituesNames = [];
   this.getachievement()

       }
  goBack(){
        this.navCtrl.pop();
    }
  goToAddAchievement(){
  	console.log('add achievement page')
    this.navCtrl.push(AddAchievementPage,  { data: {'StudentName':this.Student.StudentName,'idstudent':this.Student.idstudent}});
  }
getachievement(){
  console.log(this.URL+"achievementbystudentid/"+this.Student.idstudent);

  //this.http.get(this.URL+"achievementbystudentid/"+this.Student.idstudent+"/"+localStorage.getItem('token'))
  this.http.get(this.URL+"achievementbystudentid/"+localStorage.getItem('idstudent')+"/"+localStorage.getItem('token'))
  .map(res => res.json())
  .subscribe(data =>
  {this.ListAchievement=[];
   data.forEach(x => this.ListAchievement.unshift(x));

   console.log("this.ListAchievement",this.ListAchievement);
  console.log("****************"   ,data)}
//ListAchievement=json.parse(ListAchievement)
  ,
    err => {
      let alert = this.AlertC.create(
        {
          title:"ERROR",
          message:err,
          buttons: [
            {
              text:"OK",
              role:"cancel"

            }
          ]
        }
      );
      alert.present();
    })


}

createPdf() {
  var items = this.ListAchievement.map(function(item) {
       return [item.AchievDate ?item.AchievDate:"N/A",
        item.Program?item.Program:"N/A",
       item.Surah?item.Surah:"N/A",
       item.FromVerse?item.FromVerse:"N/A",
        item.ToVerse?item.ToVerse:"N/A",
        item.Evaluation?item.Evaluation:"N/A",
         item.Notes?item.Notes:"N/A"];
   });
  var docDefinition = {
    content:{
                style: 'itemsTable',
                table: {
                    //widths: ['*', 75, 75],
                    body: [
                        [
                            { text: 'AchievDate', style: 'itemsTableHeader' },
                            { text: 'Program', style: 'itemsTableHeader' },
                            { text: 'Surah', Surah: 'itemsTableHeader' },
                            { text: 'FromVerse', Surah: 'itemsTableHeader' },
                            { text: 'ToVerse', Surah: 'itemsTableHeader' },
                            { text: 'Evaluation', Surah: 'itemsTableHeader' },
                            { text: 'Notes', Surah: 'itemsTableHeader' }                        ]
                    ].concat(items)
                }
            }
    }

  this.pdfObj = pdfMake.createPdf(docDefinition);

   if (this.plt.is('cordova')) {
      this.pdfObj.getBuffer((buffer) => {
        var blob = new Blob([buffer], { type: 'application/pdf' });

        // Save the PDF to the data Directory of our App
        this.file.writeFile(this.file.dataDirectory, 'achievement.pdf', blob, { replace: true }).then(fileEntry => {
          // Open the PDf with the correct OS tools
          this.fileOpener.open(this.file.dataDirectory + 'achievement.pdf', 'application/pdf');
        })
      });
    } else {
      // On a browser simply use download!
      this.pdfObj.download();
    }
  //this.pdfObj.download();

}
//
// downloadPdf() {
//   if (this.plt.is('cordova')) {
//     this.pdfObj.getBuffer((buffer) => {
//       var blob = new Blob([buffer], { type: 'application/pdf' });
//
//       // Save the PDF to the data Directory of our App
//       this.file.writeFile(this.file.dataDirectory, 'myletter.pdf', blob, { replace: true }).then(fileEntry => {
//         // Open the PDf with the correct OS tools
//         this.fileOpener.open(this.file.dataDirectory + 'myletter.pdf', 'application/pdf');
//       })
//     });
//   } else {
//     // On a browser simply use download!
//     this.pdfObj.download();
//   }
// }


}

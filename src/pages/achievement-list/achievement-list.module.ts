import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AchievementListPage } from './achievement-list';

@NgModule({
  declarations: [
    AchievementListPage,
  ],
  imports: [
    IonicPageModule.forChild(AchievementListPage),
  ],
})
export class AchievementListPageModule {}

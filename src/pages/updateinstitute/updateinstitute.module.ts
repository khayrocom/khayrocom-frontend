import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateinstitutePage } from './updateinstitute';

@NgModule({
  declarations: [
    UpdateinstitutePage,
  ],
  imports: [
    IonicPageModule.forChild(UpdateinstitutePage),
  ],
})
export class UpdateinstitutePageModule {}

import { Component } from '@angular/core';
import {env} from '../../environnement'
import 'rxjs/add/operator/map';
import {AlertController, IonicPage, LoadingController, NavController, NavParams,MenuController } from 'ionic-angular';
import {Http,Response} from "@angular/http";
/**
 * Generated class for the UpdateinstitutePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-updateinstitute',
  templateUrl: 'updateinstitute.html',
})
export class UpdateinstitutePage {
  URL=env.URL;
  idinstitute:any;
  institute:any[]=[];
  constructor(private LoadincC:LoadingController,
    private http:Http,
    private AlertC:AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
  private menu: MenuController) {

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdateinstitutePage');
    this.idinstitute = this.navParams.get('data');
    console.log('this.idinstitute ',this.idinstitute );

    this.http.get(this.URL+"instinfo?id="+this.idinstitute+"&token="+localStorage.getItem('token'))
    .map(res => res.json())
    .subscribe(data =>{
      this.institute=data;
console.log('update',data);
    });
  }
  updateinstitute(idinstitute:any){


    const confirm = this.AlertC.create({
     title: 'Confirmation Alert',
     message: 'do you really want to change the information about this institute?',
     buttons: [
       {
         text: 'cancel',
         handler: () => {
           console.log('Disagree clicked');

         }
       },
       {
         text: 'Agree',
         handler: () => {
           console.log('Agree clicked');
    this.http.put(this.URL+"instupdate?id="+idinstitute+"&token="+localStorage.getItem('token'),this.institute)
    .subscribe(data=>{
      console.log('is institute updated',data);
    });
  console.log('institute updated=',this.institute);
  this.navCtrl.pop();

}
}
]
});
confirm.present();


}

}

import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';

import {Http} from "@angular/http";
import {AddUserPage} from "../add-user/add-user";
import {AchievementListPage} from "../achievement-list/achievement-list";
import {UpdatestudentPage} from "../updatestudent/updatestudent";

import 'rxjs/add/operator/map';
import {env} from '../../environnement'
import {RegistrationPage} from "../registration/registration";
import {InstituteListPage} from "../institute-list/institute-list";
import {AuthProvider} from "../../providers/auth/auth";
/**
 * Generated class for the UserListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-list',
  templateUrl: 'user-list.html',
})
export class UserListPage {
  URL=env.URL;
  public isSearchbarOpened=false;

  data: any =[];
    ListStudentNames : any [] = [];
    ListUser : any [] = [];
Class:any;

  public category: string = 'Patient';
  public categories: Array<string> = ['Patient', 'Employee'];

constructor(private LoadincC:LoadingController,
  private http:Http,
  private AlertC:AlertController,
  public navCtrl: NavController,
  public navParams: NavParams,
            public auth:AuthProvider) {
      //  this.Class = navParams.get('data');
    //console.log("class name and id Nav params",this.Class);
    //localStorage.setItem("idclass",this.Class.idclass);

  }
  segmentChanged(ev: any) {
  console.log('Segment changed', ev);
}
  goBack(){
        this.navCtrl.pop();
    }


     ionViewWillEnter() {
         this.auth.checkuserpermission.next('false');

    //this.ListInstituesNames = [];
    this.getuser();

        }

        onSearch(ev) {
           this.http.get(this.URL+"studentsearchbyclass?q="+ev.target.value+"&idclass="+localStorage.getItem('idclass')).map(res => res.json())
           .subscribe(data =>
           {    let filtredlistname:any[]=[];

             data.forEach(x => {console.log("result",x);

             filtredlistname.unshift({StudentName:x.StudentName,idstudent:x._id,Status:x.Status});

           });
           this.ListStudentNames=filtredlistname;

         })

        //  console.log(ev.target.value);
        }

onTabChanged(tabName:any) {
    this.category = tabName;
  }
  goToAddUser(){
  	console.log('add user page');
    this.navCtrl.push(RegistrationPage);
  }
    GoToInstitutes(username:any,iduser:any){
    this.auth.checkuserpermission.next('true');
    localStorage.setItem('adminlookforuserpermission',iduser);
    this.navCtrl.push(InstituteListPage,
      { data: {'username':username,'iduser':iduser}});
  }
  getuser(){
        console.log(this.URL+"allusers/"+localStorage.getItem('token'));
       this.http.get(this.URL+"allusers/"+localStorage.getItem('token'))
       .map(res => res.json())
       .subscribe(data =>
       {this.ListUser=[];
   data.forEach(x =>   {this.ListUser.unshift(x) });

         console.log("this.ListStudentNames",this.ListUser);
       console.log("****************"   ,data)},
          err => {
            let alert = this.AlertC.create(
              {
                title:"ERROR",
                message:err,
                buttons: [
                  {
                    text:"OK",
                    role:"cancel"

                  }
                ]
              }
            );
            alert.present();
          })

        //.map((res:Response ) =>  res.json())//.subscribe(data => { console.log("****************"   ,data);})
  }


archiveItem(iduser:any){
  this.http.put(this.URL+"stupdate?id="+iduser+"&token="+localStorage.getItem('token'),{archive:"true"})
      .subscribe(data=>{
    console.log('is student updated archive',data);
    this.getuser();
  })

}

    editItem(iduser:any){
        this.navCtrl.push(UpdatestudentPage,
            { data: iduser});

    }
    removeItem(iduser:any){
        console.log('user will be deleted');
        let loader = this.LoadincC.create({ content: "deleting" ,duration: 1000});
        this.http.delete(this.URL+"users?iduser="+iduser+"&token="+localStorage.getItem('token'))
            .subscribe(data=>{
                    console.log('is user deleted',data);
                loader.present();
                loader.dismissAll();
                this.ionViewWillEnter();
                this.http.put(this.URL+"instdeleteuser?token="+localStorage.getItem('token'),{iduser:iduser})
                    .subscribe(data=>{
                        console.log('is institute updated',data);
                    });
                },err => {
            let alert = this.AlertC.create(
                {
                    title:"ERROR",
                    message:err,
                    buttons: [
                        {
                            text:"OK",
                            role:"cancel"

                        }
                    ]
                }
            );
            alert.present();
        });


    }
}


import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserListPage } from './user-list';
import { SwipeSegmentDirective } from '../../directives/swipe-segment/swipe-segment';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    UserListPage//,SwipeSegmentDirective
  ],
  imports: [
    IonicPageModule.forChild(UserListPage),DirectivesModule
  ]

})
export class UserListPageModule {}

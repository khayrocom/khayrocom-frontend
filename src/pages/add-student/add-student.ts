
import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController, IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';

import { Http } from "@angular/http";

import { UserListPage } from "../user-list/user-list";

import { env } from '../../environnement'
import {StudentListPage} from "../student-list/student-list";

/**
 * Generated class for the AddStudentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-student',
  templateUrl: 'add-student.html',
})
export class AddStudentPage {
  Classes:any []=[];
  URL = env.URL;
//thisStatus=false;
  Class: any;
  StudentName: any;
  Gender: any;
  Status: any;
  RoomNumber: any;
  Level: any;
  EntryDate: any;
  ExitDate: any;
  ListClass:any;
  Halaqa:any;
  constructor(private LoadincC: LoadingController, private http: Http, private AlertC: AlertController,
              public navCtrl: NavController, public navParams: NavParams) {
    this.Halaqa = navParams.get('data');
    console.log("Institue name Nav params", this.Halaqa);


    this.http.get(this.URL + "classbyinstituteid/" + localStorage.getItem('Institute') + "/" + localStorage.getItem('token'))
        .map(res => res.json())
        .subscribe(data => {
              this.ListClass = [];
              data.forEach(x => this.ListClass.unshift({ClassName: x.ClassName, idclass: x._id}));

              console.log("this.ListClassNames", this.ListClass);
              console.log("****************", data)
            },
            err => {
              let alert = this.AlertC.create(
                  {
                    title: "ERROR",
                    message: err,
                    buttons: [
                      {
                        text: "OK",
                        role: "cancel"

                      }
                    ]
                  }
              );
              alert.present();
            })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddUserPage');
  }
  goBack() {
    this.navCtrl.pop();
  }
  goToUserList() {
    console.log('user  list page');
    this.navCtrl.push(StudentListPage, { data: { 'ClassName': this.Class.ClassName, 'idclass': this.Class.idclass } });
  }
  save() {

    let loader = this.LoadincC.create({ content: "Loading" });
    loader.present().then(() => {
      this.http.post(this.URL + "student", {
        StudentName: this.StudentName, Gender: this.Gender, Status: this.Status,
        RoomNumber: this.RoomNumber, Level: this.Level, EntryDate: this.EntryDate,
        ExitDate: this.ExitDate, idclass: this.Classes,iduser:localStorage.getItem('iduser'),
        token: localStorage.getItem('token'),idhalaqa:this.Halaqa.idhalaqa
      }).subscribe(
          data => {
            console.log(data);
            loader.dismissAll();
            this.navCtrl.pop();

          },
          err => {
            let alert = this.AlertC.create(
                {
                  title: "ERROR",
                  message: err,
                  buttons: [
                    {
                      text: "OK",
                      role: "cancel"
                    }
                  ]
                }
            );
            alert.present();
          }
      )
    })
  }
}

